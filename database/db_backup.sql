CREATE DATABASE  IF NOT EXISTS `main_database` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `main_database`;
-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: main_database
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `CHARACTERS`
--

DROP TABLE IF EXISTS `CHARACTERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CHARACTERS` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `classes` int NOT NULL,
  `races` int NOT NULL,
  `health` double NOT NULL,
  `damage` double NOT NULL,
  `critical` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_classes_idx` (`classes`),
  KEY `FK_races_idx` (`races`),
  CONSTRAINT `FK_classes` FOREIGN KEY (`classes`) REFERENCES `CLASSES` (`id`),
  CONSTRAINT `FK_races` FOREIGN KEY (`races`) REFERENCES `RACES` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CHARACTERS`
--

LOCK TABLES `CHARACTERS` WRITE;
/*!40000 ALTER TABLE `CHARACTERS` DISABLE KEYS */;
INSERT INTO `CHARACTERS` VALUES (1,'stracKz',1,3,600,80,45),(2,'Nelle',2,1,500,150,55);
/*!40000 ALTER TABLE `CHARACTERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLASSES`
--

DROP TABLE IF EXISTS `CLASSES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLASSES` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `health_multiplier` double NOT NULL,
  `damage_multiplier` double NOT NULL,
  `critical_multiplier` double NOT NULL,
  `image` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLASSES`
--

LOCK TABLES `CLASSES` WRITE;
/*!40000 ALTER TABLE `CLASSES` DISABLE KEYS */;
INSERT INTO `CLASSES` VALUES (1,'Death Knight',450,65,20,'https://i.imgur.com/RuZZ42X.jpg'),(2,'Warlock',300,80,30,'https://i.imgur.com/YbX985i.jpg'),(3,'Shaman',350,70,60,'https://i.imgur.com/MB9ZeF6.jpg');
/*!40000 ALTER TABLE `CLASSES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RACES`
--

DROP TABLE IF EXISTS `RACES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RACES` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `health_multiplier` double NOT NULL,
  `damage_multiplier` double NOT NULL,
  `critical_multiplier` double NOT NULL,
  `image` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RACES`
--

LOCK TABLES `RACES` WRITE;
/*!40000 ALTER TABLE `RACES` DISABLE KEYS */;
INSERT INTO `RACES` VALUES (1,'Human',200,10,20,'https://static.wikia.nocookie.net/wowpedia/images/0/0e/Human_Crest.png/revision/latest?cb=20180710000953'),(2,'Orc',300,5,15,'https://static.wikia.nocookie.net/wowpedia/images/7/74/Orc_Crest.png/revision/latest/scale-to-width-down/280?cb=20151113054044'),(3,'Void Elf',150,15,25,'https://static.wikia.nocookie.net/wowpedia/images/e/e0/Void_Elf_Crest.png/revision/latest?cb=20180206133618');
/*!40000 ALTER TABLE `RACES` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-20 16:24:23
