<?php

namespace App\Controller;

use App\Entity\Classes;
use App\Entity\Races;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GameplayController extends AbstractController
{
    /* --------------------------------- ROUTES --------------------------------- */
    #[Route('/gameplay', name: 'gameplay')]
    public function index(ManagerRegistry $doctrine): Response
    {
        return $this->render('pages/gameplay/index.html.twig', [
            'controller_name' => 'GameplayController',
            'pages' => HomeController::getPages(),
            'classes' => $this->getClasses($doctrine),
            'races' => $this->getRaces($doctrine),
        ]);
    }

    #[Route('/gameplay/class/{id}', name: 'gameplay_class')]
    public function index_class(ManagerRegistry $doctrine, int $id): Response
    {
        return $this->render('pages/gameplay/index_class.html.twig', [
            'controller_name' => 'GameplayController',
            'pages' => HomeController::getPages(),
            'class' => $this->getClass($doctrine, $id)
        ]);
    }

    #[Route('/gameplay/race/{id}', name: 'gameplay_race')]
    public function index_race(ManagerRegistry $doctrine, int $id): Response
    {
        return $this->render('pages/gameplay/index_race.html.twig', [
            'controller_name' => 'GameplayController',
            'pages' => HomeController::getPages(),
            'race' => $this->getRace($doctrine, $id)
        ]);
    }

    /* --------------------------------- METHODS -------------------------------- */
    public function getClasses(ManagerRegistry $doctrine): array 
    {
        return $doctrine->getRepository(Classes::class)->findAll();
    }

    public function getRaces(ManagerRegistry $doctrine): array 
    {
        return $doctrine->getRepository(Races::class)->findAll();
    }

    public function getClass(ManagerRegistry $doctrine, int $id): Classes
    {
        return $doctrine->getRepository(Classes::class)->find($id);
    }

    public function getRace(ManagerRegistry $doctrine, int $id): Races
    {
        return $doctrine->getRepository(Races::class)->find($id);
    }
}
