<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/* ---------------------------- Controller Class ---------------------------- */

class HomeController extends AbstractController 
{   
    #[Route('/home', name: 'home')]
    public function index(): Response
    {
        return $this->render('pages/home/index.html.twig', [
            'controller_name' => 'Homepage',
            'pages' => self::getPages()
        ]);
    }

    public static function getPages(): array {
        $all_pages = [];

        foreach (array_slice(scandir('/var/www/html/app/templates/pages'), 2) as $i => $page) {
            if($page === "home") {
                array_push($all_pages, $page);

                foreach (array_slice(scandir('/var/www/html/app/templates/pages'), 2) as $i2 => $page2) {
                    if($page2 !== "home") {
                        array_push($all_pages, $page2);
                    }
                }
            }
        }

        return $all_pages; 
    }
}
