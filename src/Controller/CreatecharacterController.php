<?php
namespace App\Controller;

use App\Entity\Characters;
use App\Entity\Classes;
use App\Entity\Races;
use App\Entity\Task;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreatecharacterController extends AbstractController
{
    #[Route('/createcharacter', name: 'createcharacter')]
    public function new(Request $request, ManagerRegistry $doctrine): Response
    {
        $character = new Characters();
        $entityManager = $doctrine->getManager();

        /* ------------------------------ FORM CHOICES ------------------------------ */
        $form = $this->createFormBuilder($character)
            ->add('name', TextType::class)
            ->add('classes', ChoiceType::class, [
                'choices'  => [
                    "Death Knight" => $doctrine->getRepository(Classes::class)->find(1),
                    "Warlock" => $doctrine->getRepository(Classes::class)->find(2),
                    "Shaman" => $doctrine->getRepository(Classes::class)->find(3),
                ]
            ])
            ->add('races', ChoiceType::class, [
                'choices'  => [
                    "Human" => $doctrine->getRepository(Races::class)->find(1),
                    "Orc" => $doctrine->getRepository(Races::class)->find(2),
                    "Void Elf" => $doctrine->getRepository(Races::class)->find(3),
                ]
            ])
            ->add('submit', SubmitType::class)
            ->getForm();

        // ...

        /* ----------------- VERIFICATION IF THE FORM WAS SUBMITTED ----------------- */
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            // Sets the stats of the character based on the class and race stats
            $this->finalStats($form->getData());

            $entityManager->persist($character);
            $entityManager->flush();

            // Redirect to the character's page
            return $this->redirectToRoute('characters');
        }

        /* ---------- RENDER A FORM PAGE IF THE FORM WAS NOT YET SUBMITTED ---------- */
        return $this->renderForm('pages/characters/index_create.html.twig', [
            'form' => $form,
            'pages' => HomeController::getPages()
        ]);
    }

    /* --------------------------------- METHODS -------------------------------- */
    public function finalStats($character): void {
        $character->setHealth($character->getClasses()->getHealthMultiplier() + $character->getRaces()->getHealthMultiplier());
        $character->setDamage($character->getClasses()->getDamageMultiplier() + $character->getRaces()->getDamageMultiplier());
        $character->setCritical($character->getClasses()->getCriticalMultiplier() + $character->getRaces()->getCriticalMultiplier());
    }
}