<?php

namespace App\Controller;

use App\Entity\Characters;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CharactersController extends AbstractController
{
    #[Route('/characters', name: 'characters')]
    public function index(Request $request, ManagerRegistry $doctrine): Response
    {
        /* ------------------------------ UPDATE MANAGEMENT ------------------------------ */
        $request = Request::createFromGlobals();

        if($request->request->get('id') !== null) {
            $this->updateCharacter($doctrine, $request);

            // REDIRECT TO REMOVE THE POST STATUS
            return $this->redirectToRoute('characters');
        }
        
        return $this->render('pages/characters/index.html.twig', [
            'controller_name' => 'CharactersController',
            'pages' => HomeController::getPages(),
            'characters' => $this->getCharacters($doctrine)
        ]);
    }

    #[Route('/characters/delete/{id}', name: 'characters_delete')]
    public function index_delete(ManagerRegistry $doctrine, int $id): Response
    {
        $this->deleteCharacter($doctrine, $id);

        return $this->render('pages/characters/index.html.twig', [
            'controller_name' => 'CharactersController',
            'pages' => HomeController::getPages(),
            'characters' => $this->getCharacters($doctrine)
        ]);
    }

    public function getCharacters(ManagerRegistry $doctrine):array {
        return $doctrine->getRepository(Characters::class)->findAll();
    }
    
    public function deleteCharacter(ManagerRegistry $doctrine, int $id): void {
        $entityManager = $doctrine->getManager();

        $specific_character = $doctrine->getRepository(Characters::class)->find($id);

        $entityManager->remove($specific_character);
        $entityManager->flush();
    }

    public function updateCharacter(ManagerRegistry $doctrine, Request $request): void {
        $entityManager = $doctrine->getManager();

        $specific_character = $doctrine->getRepository(Characters::class)->find($request->request->get('id'));

        $specific_character->setName($request->request->get('name'));
        $specific_character->setHealth($request->request->get('health'));
        $specific_character->setDamage($request->request->get('damage'));
        $specific_character->setCritical($request->request->get('critical'));

        $entityManager->flush();
    }
}
