<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Characters
 *
 * @ORM\Table(name="CHARACTERS", indexes={@ORM\Index(name="FK_classes_idx", columns={"classes"}), @ORM\Index(name="FK_races_idx", columns={"races"})})
 * @ORM\Entity(repositoryClass="App\Repository\CharactersRepository")
 */
class Characters
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="health", type="float", precision=10, scale=0, nullable=false)
     */
    private $health;

    /**
     * @var float
     *
     * @ORM\Column(name="damage", type="float", precision=10, scale=0, nullable=false)
     */
    private $damage;

    /**
     * @var float
     *
     * @ORM\Column(name="critical", type="float", precision=10, scale=0, nullable=false)
     */
    private $critical;

    /**
     * @var \Classes
     *
     * @ORM\ManyToOne(targetEntity="Classes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="classes", referencedColumnName="id")
     * })
     */
    private $classes;

    /**
     * @var \Races
     *
     * @ORM\ManyToOne(targetEntity="Races")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="races", referencedColumnName="id")
     * })
     */
    private $races;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getHealth(): ?float
    {
        return $this->health;
    }

    public function setHealth(float $health): self
    {
        $this->health = $health;

        return $this;
    }

    public function getDamage(): ?float
    {
        return $this->damage;
    }

    public function setDamage(float $damage): self
    {
        $this->damage = $damage;

        return $this;
    }

    public function getCritical(): ?float
    {
        return $this->critical;
    }

    public function setCritical(float $critical): self
    {
        $this->critical = $critical;

        return $this;
    }

    public function getClasses(): ?Classes
    {
        return $this->classes;
    }

    public function setClasses(?Classes $classes): self
    {
        $this->classes = $classes;

        return $this;
    }

    public function getRaces(): ?Races
    {
        return $this->races;
    }

    public function setRaces(?Races $races): self
    {
        $this->races = $races;

        return $this;
    }


}
