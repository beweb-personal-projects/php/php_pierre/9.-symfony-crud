<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Races
 *
 * @ORM\Table(name="RACES")
 * @ORM\Entity(repositoryClass="App\Repository\RacesRepository")
 */
class Races
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="health_multiplier", type="float", precision=10, scale=0, nullable=false)
     */
    private $healthMultiplier;

    /**
     * @var float
     *
     * @ORM\Column(name="damage_multiplier", type="float", precision=10, scale=0, nullable=false)
     */
    private $damageMultiplier;

    /**
     * @var float
     *
     * @ORM\Column(name="critical_multiplier", type="float", precision=10, scale=0, nullable=false)
     */
    private $criticalMultiplier;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="text", length=0, nullable=false)
     */
    private $image;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getHealthMultiplier(): ?float
    {
        return $this->healthMultiplier;
    }

    public function setHealthMultiplier(float $healthMultiplier): self
    {
        $this->healthMultiplier = $healthMultiplier;

        return $this;
    }

    public function getDamageMultiplier(): ?float
    {
        return $this->damageMultiplier;
    }

    public function setDamageMultiplier(float $damageMultiplier): self
    {
        $this->damageMultiplier = $damageMultiplier;

        return $this;
    }

    public function getCriticalMultiplier(): ?float
    {
        return $this->criticalMultiplier;
    }

    public function setCriticalMultiplier(float $criticalMultiplier): self
    {
        $this->criticalMultiplier = $criticalMultiplier;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getRacePercentages(): array 
    {
        return [
            "health_percentage" => ($this->getHealthMultiplier() / 500) * 100, 
            "damage_percentage" => ($this->getDamageMultiplier() / 100) * 100, 
            "critical_percentage" => ($this->getCriticalMultiplier() / 100) * 100, 
        ];
    }
}
